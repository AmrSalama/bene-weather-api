module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users_cities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: { tableName: 'users' },
          key: 'id'
        }
      },
      city_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: { tableName: 'cities' },
          key: 'id'
        }
      }
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('users_cities');
  }
};
