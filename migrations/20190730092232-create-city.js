module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('cities', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        external_id: {
          allowNull: false,
          type: Sequelize.INTEGER
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING(80)
        },
        country: {
          allowNull: false,
          type: Sequelize.STRING(2)
        }
      })
      .then(() => queryInterface.addIndex('cities', ['external_id']));
  },
  down: queryInterface => {
    return queryInterface.dropTable('cities');
  }
};
