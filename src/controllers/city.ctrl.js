const $baseAction = require('./$baseAction');
const { APIResponse } = require('../utils');
const { OpenWeatherMapService } = require('../services');
const { User, City, UserCity, Sequelize, sequelize } = require('../models');

module.exports = {
  // list user's cities
  list: $baseAction(async (req, res) => {
    const userId = req.authUserId;
    const user = await User.findByPk(userId, {
      include: [{ model: City, as: 'cities', through: { attributes: [] } }]
    });
    const { cities } = user;

    return APIResponse.Ok(res, cities);
  }),

  // add new city to user's list
  add: $baseAction(['cityId'], async (req, res) => {
    const { cityId } = req.body;
    const userId = req.authUserId;

    if (!Number.isInteger(+cityId)) return APIResponse.BadRequest(res);

    // 1. check if city id is valid
    const city = await City.findByPk(+cityId);
    if (!city) return APIResponse.BadRequest(res, 'Invalid City Id');

    // 2. check if city already in user's list
    const userCities = await User.findByPk(userId, {
      include: [{ model: City, as: 'cities', where: { id: cityId } }]
    });

    // 3. if not exists in user's list > insert
    if (!userCities)
      await UserCity.create({ user_id: userId, city_id: cityId });

    return APIResponse.NoContent(res);
  }),

  // get city weather
  get: $baseAction(async (req, res) => {
    const { id: cityId } = req.params;
    const userId = req.authUserId;

    if (!Number.isInteger(+cityId)) return APIResponse.NotFound(res);

    // 1. check if city exists
    const city = await City.findByPk(+cityId);
    if (!city) return APIResponse.NotFound(res);

    // 2. check if city already in user's list
    const userCities = await User.findByPk(userId, {
      include: [{ model: City, as: 'cities', where: { id: cityId } }]
    });
    if (!userCities) return APIResponse.Forbidden(res);

    // 3. call Open Weather Map service
    const weather = await OpenWeatherMapService.fetchWeather(city.external_id);

    return APIResponse.Ok(res, weather);
  }),

  // search cities
  search: $baseAction(async (req, res) => {
    const { q, limit = 10 } = req.query;
    const userId = req.authUserId;

    // empty keyword => []
    if (q.length === 0) return APIResponse.Ok(res, []);

    // fetch user cities to filter them out from search results
    const user = await User.findByPk(userId, {
      include: [{ model: City, as: 'cities', through: { attributes: [] } }]
    });
    const userCitiesIds = user.cities.map(city => city.id);

    const suggestions = await City.findAll({
      where: Sequelize.and(
        // match keyword
        sequelize.where(sequelize.fn('LOWER', sequelize.col('name')), {
          [Sequelize.Op.like]: `%${(q || '').toLocaleLowerCase()}%`
        }),
        // not in user cities list
        { id: { [Sequelize.Op.notIn]: userCitiesIds } }
      ),
      order: sequelize.col('name'),
      limit
    });

    return APIResponse.Ok(res, suggestions);
  })
};
