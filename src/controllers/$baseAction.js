const { APIResponse } = require('../utils');

// Provide base action required behaviors
// e.g. handle async rejections
//
// Usage examples:
// basic usage: passing the request handler
//    $baseAction((req, res) => {})
//
// check body fields: if you want to check if body contains required fields
//    $baseAction(['field1', 'field2'], (req, res) => {})
module.exports = (a, b) => {
  let action = null;
  let requiredBodyFields = null;

  // usage example 1
  if (b === undefined) action = a;
  // usage example 2
  else {
    requiredBodyFields = a;
    action = b;
  }

  return (req, res, next) => {
    if (requiredBodyFields) {
      const badRequestErrors = [];
      const bodyFields = Object.keys(req.body);
      requiredBodyFields.forEach(field => {
        if (bodyFields.indexOf(field) === -1) {
          badRequestErrors.push({ field, message: 'required field' });
        }
      });

      if (badRequestErrors.length) {
        return APIResponse.BadRequest(res, { errors: badRequestErrors });
      }
    }

    return action(req, res, next).catch(err => next(err));
  };
};
