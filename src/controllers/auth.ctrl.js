const $baseAction = require('./$baseAction');
const { APIResponse } = require('../utils');
const { User } = require('../models');

module.exports = {
  login: $baseAction(['username', 'password'], async (req, res) => {
    const { username, password } = req.body;
    const invalidUserMsg = 'Invalid Username or Password';

    // 1. find user with provided email
    const user = (await User.findAll({
      limit: 1,
      where: { email: username }
    }))[0];
    if (!user) return APIResponse.Unauthorized(res, invalidUserMsg);

    // 2. check if password is valid
    const isValidPassword = await user.isValidPassword(password);
    if (!isValidPassword) return APIResponse.Unauthorized(res, invalidUserMsg);

    // 3. generate token
    const token = await user.generateToken();

    return APIResponse.Ok(res, { ...user.toJSON(), token });
  })
};
