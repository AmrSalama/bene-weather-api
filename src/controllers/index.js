const auth = require('./auth.ctrl');
const city = require('./city.ctrl');

module.exports = {
  auth,
  city
};
