const { Unauthorized } = require('../utils').APIResponse;

module.exports = (req, res, next) => {
  if (!req.authUserId) return Unauthorized(res);
  return next();
};
