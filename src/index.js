require('dotenv').config();
const database = require('./database');
const server = require('./server');

// connect database > run server > enjoy
database
  .connect()
  .then(server.run)
  .then(() => console.info('🕹  Enjoy! 😚'))
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
