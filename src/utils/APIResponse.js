module.exports = {
  // 200 Ok
  Ok: (res, data = {}) => res.status(200).json(data),

  // 201 Ok
  Created: (res, data = {}) => res.status(201).json(data),

  // 204 No Content
  NoContent: res => res.status(204).json(),

  // 400 Bad request
  BadRequest: (res, data = 'Bad Request') =>
    res.status(400).json(typeof data === 'string' ? { message: data } : data),

  // 401 Unauthorized
  Unauthorized: (res, data = 'Unauthorized Access') =>
    res.status(401).json(typeof data === 'string' ? { message: data } : data),

  // 403 Forbidden
  Forbidden: (res, data = 'Forbidden Access') =>
    res.status(403).json(typeof data === 'string' ? { message: data } : data),

  // 404 Not found
  NotFound: (res, data = 'Not Found') =>
    res.status(404).json(typeof data === 'string' ? { message: data } : data),

  // 415 Unsupported media type
  UnsupportedMediaType: (res, data = 'Unsupported media type') =>
    res.status(415).json(typeof data === 'string' ? { message: data } : data),

  // 422 Unprocessable Entity
  UnprocessableEntity: (res, errors) => res.status(422).json({ errors }),

  // 500 Server error
  ServerError: (res, message = 'Internal Server Error') =>
    res.status(500).json({ message })
};
