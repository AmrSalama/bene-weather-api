module.exports = (sequelize, DataTypes) => {
  const UserCity = sequelize.define(
    'UserCity',
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      city_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      }
    },
    { tableName: 'users_cities' }
  );

  UserCity.associate = () => {};

  return UserCity;
};
