module.exports = (sequelize, DataTypes) => {
  const City = sequelize.define(
    'City',
    {
      external_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      country: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    { tableName: 'cities' }
  );

  // relationships
  City.associate = models => {
    City.belongsToMany(models.User, {
      through: 'UserCity',
      as: 'users',
      foreignKey: 'city_id',
      otherKey: 'user_id'
    });
  };

  // hide external id
  City.prototype.toJSON = function toJSON() {
    const city = { ...this.get() };
    delete city.external_id;
    return city;
  };

  return City;
};
