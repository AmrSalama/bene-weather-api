const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: { isEmail: true }
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    { tableName: 'users' }
  );

  // relationships
  User.associate = models => {
    User.belongsToMany(models.City, {
      through: 'UserCity',
      as: 'cities',
      foreignKey: 'user_id',
      otherKey: 'city_id'
    });
  };

  // encrpt password before save (if changed)
  User.beforeSave((user, options) => {
    if (options.fields.indexOf('password') === -1) return Promise.resolve();
    return bcrypt.hash('password', 10).then(hashed => {
      // eslint-disable-next-line no-param-reassign
      user.password = hashed;
    });
  });

  // instance method to check password
  User.prototype.isValidPassword = function isValidPassword(password) {
    return bcrypt.compare(password, this.password);
  };

  // instance method to check password
  User.prototype.generateToken = function generateToken() {
    const payload = { userId: this.id };
    const options = { expiresIn: '360h' }; // 15 days
    return new Promise((resolve, reject) => {
      jwt.sign(payload, process.env.JWT_SECRET, options, (err, token) => {
        if (err) return reject(err);
        return resolve(token);
      });
    });
  };

  // hide password
  User.prototype.toJSON = function toJSON() {
    const user = { ...this.get() };
    delete user.password;
    return user;
  };

  return User;
};
