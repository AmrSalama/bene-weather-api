const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const helmet = require('helmet');

const middlewares = require('./middlewares');
const routes = require('./routes');

const logger =
  process.env.NODE_ENV === 'development'
    ? morgan('dev')
    : morgan('combined', { skip: (_, res) => res.statusCode < 500 });

// express middlewares stack
const app = express();
app.use(logger);
app.use(express.json({ limit: '5mb' }));
app.use(cors());
app.use(helmet());

app.use(middlewares.ensureContentType);
app.use(middlewares.jwtAuthChecker);
app.use(routes);
app.use(middlewares.errorHandler);

module.exports = {
  run: () =>
    new Promise((resolve, reject) => {
      const port = process.env.PORT || 5000;
      const server = app.listen(port);

      server.once('listening', () => {
        console.info(`🤟 Server is listening at port ${port}`);
        resolve(server);
      });

      server.once('error', err => {
        console.error(`🤔 Server failed to listen at port ${port}`);
        reject(err);
      });
    })
};
