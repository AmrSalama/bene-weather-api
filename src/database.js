const models = require('./models');

module.exports = {
  connect: () =>
    new Promise((resolve, reject) => {
      models.sequelize
        .authenticate()
        .then(() => {
          console.log('🤟 Successfully connected to database');
          resolve();
        })
        .catch(err => {
          console.error('🤔 Failed to connect to database');
          reject(err);
        });
    })
};
