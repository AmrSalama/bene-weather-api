const axios = require('axios');

const OWM_BASE = 'https://api.openweathermap.org/data/2.5/weather';
const APPID = process.env.OPEN_WEATHER_MAP_APP_ID;

module.exports = {
  fetchWeather: async cityId => {
    const url = `${OWM_BASE}?id=${cityId}&appid=${APPID}&units=metric`;
    const response = await axios.get(url);

    // [TODO] enhnace unsuccessful reponse
    if (response.status !== 200) throw Error('Something Went Wrong');

    const { data } = response;
    const { name, timezone } = data;
    const { country, sunrise, sunset } = data.sys;
    const { id: weatherId, description } = data.weather[0];
    const { temp } = data.main;

    const weather = {
      name,
      country,
      timezone,
      sunrise,
      sunset,
      temp,
      weatherId,
      description
    };

    return weather;
  }
};
