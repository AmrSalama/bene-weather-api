const express = require('express');
const { NotFound } = require('../utils').APIResponse;
const { isAuthenticated } = require('../policies');
const ctrls = require('../controllers');

const router = express.Router();
router.post('/login', ctrls.auth.login);
router.get('/cities', isAuthenticated, ctrls.city.list);
router.post('/cities', isAuthenticated, ctrls.city.add);
router.get('/cities/search', isAuthenticated, ctrls.city.search);
router.get('/cities/:id', isAuthenticated, ctrls.city.get);
router.all('*', (req, res) => NotFound(res));

module.exports = router;
