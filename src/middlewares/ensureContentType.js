const { UnsupportedMediaType } = require('../utils').APIResponse;

// Ensure content-type of the request
// ['application/json']
module.exports = (req, res, next) => {
  const contentType = req.headers['content-type'];
  if (contentType && !contentType.includes('application/json'))
    return UnsupportedMediaType(res);

  return next();
};
