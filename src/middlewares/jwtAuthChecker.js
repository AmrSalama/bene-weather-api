const jwt = require('jsonwebtoken');
const { APIResponse } = require('../utils');

// JWT Auth Checker
// This middleware validate JWT tokens only if provided
module.exports = (req, res, next) => {
  req.authUserId = null;

  let token = '';
  const { authorization: auth } = req.headers;
  if (auth) {
    if (auth.startsWith('Bearer ')) token = auth.slice(7, auth.length);
    else return APIResponse.Unauthorized(res, 'Token Should be Bearer Token');
  } else {
    return next();
  }

  return new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err) return reject(err);
      return resolve(decoded);
    });
  })
    .then(({ userId }) => {
      req.authUserId = userId;
      return next();
    })
    .catch(err => {
      if (err.name) {
        if (err.name === 'JsonWebTokenError')
          APIResponse.Unauthorized(res, 'Invalid Access Token');
        else if (err.name === 'TokenExpiredError')
          APIResponse.Unauthorized(res, 'Expired Access Token');
      } else {
        APIResponse.ServerError(res);
      }
    });
};
