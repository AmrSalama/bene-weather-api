// Server error handler
module.exports = async (err, req, res, next) => {
  if (res.headersSent) return next(err);

  const status = err.status ? +err.status : 500;
  let message = '';
  if (status >= 500) message = 'Internal Server Error';
  else message = err.message ? err.message : 'Something Went Wrong';

  if (status >= 500) console.error(err);
  return res.status(status).json({ message });
};
