// config file required only by sequelize-cli to migrate and seed

require('dotenv').config();

module.exports = {
  [process.env.NODE_ENV || 'development']: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOSTNAME,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT
  }
};
