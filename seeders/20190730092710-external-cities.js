const fs = require('fs').promises;
const path = require('path');

// seed cities table with city list provided by openweathermap
// http://bulk.openweathermap.org/sample/
// http://country.io/capital.json

module.exports = {
  up: queryInterface => {
    return Promise.all([
      fs.readFile(path.resolve(__dirname, 'external', 'city.list.min.json')),
      fs.readFile(path.resolve(__dirname, 'external', 'capitals.json'))
    ]).then(([citiesData, capitalsData]) => {
      // 1. parse data to json
      const citiesJson = JSON.parse(citiesData);
      const capitalsJson = JSON.parse(capitalsData);

      // 2. filter cities to capitals only
      // and remove duplicates: openweathermap data has many duplicates
      // https://bit.ly/2LMBgvv
      const st = new Set(); // handle duplicates
      const filteredCapitals = citiesJson.filter(({ name: city, country }) => {
        if (st.has(country) || city !== capitalsJson[country]) return false;
        st.add(country);
        return true;
      });

      // 3. map capitals for our needs
      const capitals = filteredCapitals.map(city => ({
        external_id: city.id,
        name: city.name,
        country: city.country
      }));

      return queryInterface.bulkInsert('cities', capitals);
    });
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('cities', null, {});
  }
};
