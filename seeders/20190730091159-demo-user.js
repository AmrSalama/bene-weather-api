const bcrypt = require('bcryptjs');

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('users', [
      {
        email: 'jobs@benestudio.co',
        password: bcrypt.hashSync('password', 10)
      }
    ]);
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
