module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert('users_cities', [
      { user_id: 1, city_id: 130 }
    ]);
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('users_cities', null, {});
  }
};
