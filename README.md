# Weather App | API

Bene Weather App backend API

### Tech Stack

- NodeJS
- Express
- PostgreSQL
- Sequelize ORM

### Commands

| Command                             | Description            |
| ----------------------------------- | ---------------------- |
| `npx sequelize-cli db:migrate`      | Running Migrations     |
| `npx sequelize-cli db:migrate:undo` | Undoing Migrations     |
| `npx sequelize-cli db:seed:all`     | Running Seeds          |
| `npx sequelize-cli db:seed:undo`    | Undoing Seeds          |
| `npm run dev`                       | Run Development Server |

### Exposed Endpoints

| Method | URL              | Require Auth | Description                      |
| ------ | ---------------- | ------------ | -------------------------------- |
| `POST` | `/login`         | NO           | Login with Username and Password |
| `GET`  | `/cities`        | Yes          | Fetch user stored cities         |
| `GET`  | `/cities/:id`    | Yes          | Fetch city weather information   |
| `GET`  | `/login`         | Yes          | Login with Username and Password |
| `GET`  | `/cities/search` | Yes          | Search for captials with keyword |

### Authorization

Protected endpoints requires `Authorization` header with value `Bearer token` where token is JWT token obtained from `login` endpoint
